#include<stdio.h>
void swap(int*,int*);

int main()
{
int m,n;
printf("\nenter the value of x and y\n");
scanf("%d%d",&m,&n);
printf("\nvalues before swapping\nm =%d\nn= %d",m,n);
swap(&m,&n);
printf("\nvalues after swapping\nm= %d\nn= %d\n",m,n);
return 0;
}

void swap(int *x,int *y)
{
int temp;
temp=*x;
*x=*y;
*y=temp;
}