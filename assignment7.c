#include <stdio.h>
int main()
{
 int SP[4][3],TSP[3],TSS[4],i,j;
 printf(" \n\n Read the sales of 3 products for 4 Salesmen\n");
 for(i=0;i<4;i++)
  {
   printf("\n\n Read the sales of 3 products for Salesman %d\n",i+1);
   for(j=0;j<3;j++)
    {
      scanf("%d",&SP[i][j]);
    }
  }

 for(i=0;i<4;i++)
  {
   TSS[i]=0;
   for(j=0;j<3;j++)
    {
     TSS[i]=TSS[i]+SP[i][j];
    }
   printf("\n Total Sales of Salesman %d is %d ",i+1,TSS[i]);
  }

  for(j=0;j<3;j++)
  {
   TSP[j]=0;
   for(i=0;i<4;i++)
    {
     TSP[j]=TSP[j]+SP[i][j];
    }
   printf("\n Total Sales of Product %d is %d ",j+1,TSP[j]);
  }
  return 0;
 }
