#include <stdio.h>
#include <conio.h>

void main()
  {
  int mat[5][5],trans[5][5],i,j,m,n;
  clrscr();
  printf("\n Read the Order of Matrix \n");
  scanf("%d%d",&m,&n);
  printf("\n Read the elements of Matrix \n");
  for(i=0;i<m;i++)
   {
      for(j=0;j<n;j++)
	scanf("%d",&mat[i][j]);
   }

    for(i=0;i<m;i++)
      for(j=0;j<n;j++)
       trans[j][i]=mat[i][j];
  printf("\n Transpose of the Matrix is\n ");
     for(i=0;i<m;i++)
     {
      printf("\n");
      for(j=0;j<n;j++)
      printf("%3d", trans[i][j]); 
     }
 }