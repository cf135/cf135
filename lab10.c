#include<stdio.h>
#include<conio.h>

int main()
{
  FILE *fp;
  char ch;
  clrscr();

  /* open file in write mode */
  fp=fopen("input.txt", "w");
  printf("Enter data\n");

  /* read from keyboard */
  while((ch=getchar()) != EOF)
  {
   putc(ch,fp);
  }
  fclose(fp);

  /* open file in read mode */
  fp=fopen("input.txt", "r");

  /* display on screen */
  printf("\nFile contents are: \n");
  while((ch=getc(fp)) != EOF)
  {
   printf("%c", ch);
  }
  fclose(fp);
  return (0);
}